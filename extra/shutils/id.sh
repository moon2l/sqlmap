#!/bin/bash

# $Id: id.sh 4666 2012-01-11 14:59:46Z stamparm $

# Copyright (c) 2006-2012 sqlmap developers (http://www.sqlmap.org/)
# See the file 'doc/COPYING' for copying permission

# Adds SVN property 'Id' to project files
find ../../. -type f -iname "*.py" -exec sed -i s/\$Id.*$/\$Id\$/g '{}' \;
find ../../. -type f -iname "*.py" -exec svn propset svn:keywords "Id" '{}' \;
