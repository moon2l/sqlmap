#!/usr/bin/env python

"""
$Id: sqlmap.py 4708 2012-02-01 15:04:56Z stamparm $

Copyright (c) 2006-2012 sqlmap developers (http://www.sqlmap.org/)
See the file 'doc/COPYING' for copying permission
"""

import sys

PYVERSION = sys.version.split()[0]

if PYVERSION >= "3" or PYVERSION < "2.6":
    exit("[CRITICAL] incompatible Python version detected ('%s'). For successfully running sqlmap you'll have to use version 2.6 or 2.7 (visit \"http://www.python.org/download/\")" % PYVERSION)
else:
    from _sqlmap import main
    # import needed for proper working of --profile switch
    from lib.controller.controller import start
    main()
