Files in this folder represent SQL stored procedure declarations used
by sqlmap on the target system. They are licensed under the terms of
the GNU Lesser General Public License.
