#!/usr/bin/env python

"""
$Id: takeover.py 4666 2012-01-11 14:59:46Z stamparm $

Copyright (c) 2006-2012 sqlmap developers (http://www.sqlmap.org/)
See the file 'doc/COPYING' for copying permission
"""

import re

from lib.core.agent import agent
from lib.core.common import isTechniqueAvailable
from lib.core.common import normalizePath
from lib.core.common import ntToPosixSlashes
from lib.core.common import randomStr
from lib.core.common import readInput
from lib.core.data import kb
from lib.core.data import logger
from lib.core.data import paths
from lib.core.enums import PAYLOAD
from lib.request import inject
from lib.request.connect import Connect as Request

from plugins.generic.takeover import Takeover as GenericTakeover

class Takeover(GenericTakeover):
    def __init__(self):
        self.__basedir = None
        self.__datadir = None

        GenericTakeover.__init__(self)